require "rails_helper"

RSpec.describe ProductsController, :type => :controller do
  describe "GET #validate_bonus_code" do
    it "responds with an HTTP 200 status code if sold bonus code for the product is found" do
      product = Product.create({title: 'Halo 4', bonus_codes_preloaded: true, status_preloaded: true})
      bonus_code = BonusCode.create({code: 'bd7ct34q', status: true, product_id: product.id})
      get :validate_bonus_code, product_id: product.id, code: 'bd7ct34q'
      expect(response).to have_http_status(200)
    end
    it "responds with an HTTP 404 status code if bonus code doesn’t belong to the product " do
      product = Product.create({title: 'Halo 4', bonus_codes_preloaded: true, status_preloaded: true})
      get :validate_bonus_code, product_id: product.id, code: 'bd7ct34q'
      expect(response).to have_http_status(404)
    end
    it "responds with an HTTP 403 status code if bonus code hasn’t been sold" do
      product = Product.create({title: 'Halo 4', bonus_codes_preloaded: true, status_preloaded: true})
      bonus_code = BonusCode.create({code: 'bd7ct34q', status: false, product_id: product.id})
      get :validate_bonus_code, product_id: product.id, code: 'bd7ct34q'
      expect(response).to have_http_status(403)
    end
  end
end