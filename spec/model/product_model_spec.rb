require "rails_helper"
require File.expand_path('../../../lib/bonus_code_validatable/third_party_service', __FILE__)

class NnService < ThirdPartyService
	def has_code(product, code)
		return (['bhjb3473', 'vb36xbe7', 'n3ycb3uc'].include?(code) && product.id == 1)
	end
	def validate_code(product, code)
		return (['bhjb3473', 'vb36xbe7'].include?(code) && product.id == 1)
	end
end

RSpec.describe Product, :type => :model do
  it "checks if product has code when bonus codes are loaded via third-party service" do
    product = Product.create({id: 1, title: 'Skype $10', bonus_codes_preloaded: false, status_preloaded: false, service: 'NN'})
    expect(product.has_code('bhjb3473')).to be(true)
    expect(product.has_code('bbhjb473')).to be(false)
  end
  it "validates product bonus code via third-party service" do
    product = Product.create({id: 1, title: 'Skype $10', bonus_codes_preloaded: false, status_preloaded: false, service: 'NN'})
    expect(product.validate_code('vb36xbe7')).to be(true)
    expect(product.validate_code('n3ycb3uc')).to be(false)
  end
  it "checks if product has code when bonus codes are pre-loaded" do
    product = Product.create({id: 1, title: 'Skype $10', bonus_codes_preloaded: true, status_preloaded: true})
    bonus_code = BonusCode.create({code: 'bd7ct34q', status: true, product_id: 1})
    expect(product.has_code('bd7ct34q')).to be(true)
    expect(product.has_code('bbhjb473')).to be(false)
  end
  it "validates pre-loaded product bonus code" do
    product = Product.create({id: 1, title: 'Skype $10', bonus_codes_preloaded: true, status_preloaded: true})
    sold_bonus_code = BonusCode.create({code: 'bd7ct34q', status: true, product_id: 1})
    unsold_bonus_code = BonusCode.create({code: 'n3ycb3uc', status: false, product_id: 1})

    expect(product.validate_code('bd7ct34q')).to be(true)
    expect(product.validate_code('n3ycb3uc')).to be(false)
  end
end