module BonusCodeValidatable
	load File.expand_path('../bonus_code_validatable/service_factory.rb', __FILE__)

	def has_code(code)
		if self.bonus_codes_preloaded
			return BonusCode.exists?(code: code, product_id: self.id)
		else
			return ServiceFactory.create_service(self).has_code(self, code)
		end
	end

	def validate_code(code)
		if self.status_preloaded
			bc = BonusCode.find_by_product_id_and_code(self.id, code) 
			return !!(bc && bc.status)
		end
		return ServiceFactory.create_service(self).validate_code(self, code)
	end
end