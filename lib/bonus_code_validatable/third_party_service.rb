class ThirdPartyService
	# must be redefined in subclasses
	def has_code(product, code)
		return false
	end
	# must be redefined in subclasses
	def validate_code(product, code)
		return false
	end
end