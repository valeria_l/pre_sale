load File.expand_path('../third_party_service.rb', __FILE__)
load File.expand_path('../tv_service.rb', __FILE__)
load File.expand_path('../rtg_service.rb', __FILE__)
class ServiceFactory
	def self.create_service(product)
		return (product.service.capitalize + "Service").constantize.new
	end
end