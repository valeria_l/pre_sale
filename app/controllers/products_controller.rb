class ProductsController < ApplicationController
  def validate_bonus_code
  	product = Product.find(bc_params[:product_id])
  	bonus_code = bc_params[:code]
  	return head 404 if !product.has_code(bonus_code)
  	return head 200 if product.validate_code(bonus_code)
  	return head 403
  end

  private
  def bc_params
  	params.permit(:product_id,:code)
  end
end
