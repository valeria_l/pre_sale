class AddProductIdToBonusCodes < ActiveRecord::Migration
  def change
    add_column :bonus_codes, :product_id, :integer
  end
end
