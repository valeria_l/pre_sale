class CreateBonusCodes < ActiveRecord::Migration
  def change
    create_table :bonus_codes do |t|
      t.string :code
      t.boolean :status

      t.timestamps
    end
  end
end
