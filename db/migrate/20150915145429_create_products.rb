class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.boolean :bonus_codes_preloaded
      t.boolean :status_preloaded
      t.string :service

      t.timestamps
    end
  end
end
