products = Product.create([
	{title: 'Halo 4', bonus_codes_preloaded: true, status_preloaded: true},
	{title: 'Samsung Galaxy 4', bonus_codes_preloaded: true, status_preloaded: false, service: 'TV'},
	{title: 'Skype $10', bonus_codes_preloaded: false, status_preloaded: false, service: 'RTG'}
])

bonus_codes = BonusCode.create([
	{code: 'bd7ct34q', status: true, product_id: 1},
	{code: 'cyn37ch3', status: false, product_id: 1},
	{code: 'v437cj32', product_id: 2},
	{code: 'v63ncn3y', product_id: 2},
])